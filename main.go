package main

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/developersjunior/API-REST-TRABALHO/routes"

	"gitlab.com/developersjunior/API-REST-TRABALHO/middlewares"

	// teste

	"github.com/gorilla/mux"
)

func start(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "WELlCOME here!!!")
}

func setRoutes(router *mux.Router) {
	router.HandleFunc("/", start)
	router.HandleFunc("/tasks", routes.ListTasks).Methods("GET")
	router.HandleFunc("/tasks/{id}", routes.ListTaskById).Methods("GET")
	router.HandleFunc("/tasks", routes.NewTask).Methods("POST")
	router.HandleFunc("/tasks/{id}", routes.UpdateTaskById).Methods("PUT")
	router.HandleFunc("/tasks/{id}", routes.DeleteTaskById).Methods("DELETE")
}

func main() {

	var router *mux.Router

	log.Printf("Server is working on http://localhost:3000")

	router = mux.NewRouter()

	router.Use(middlewares.JsonMiddleware)

	setRoutes(router)

	err := http.ListenAndServe(":3000", router)
	if err != nil {
		fmt.Println("Error", err)
	}
}
