package routes

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/developersjunior/API-REST-TRABALHO/database"

	"fmt"

	"github.com/gorilla/mux"
)

type Task struct {
	ID          int    `json:"id"`
	Title       string `json:title`
	Description string `json:"description"`
}

var tasks []Task

func NewTask(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	conn := database.SetConnection()
	defer conn.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		fmt.Fprint(w, "Bad Request!")
	}

	json.Unmarshal(body, &register)

	action, err := conn.Prepare("INSERT INTO task (title, description) values(?, ?)")
	action.Exec(register.Title, register.Description)

	encoder := json.NewEncoder(w)
	encoder.Encode(register)
}

func ListTasks(w http.ResponseWriter, r *http.Request) {
	conn := database.SetConnection()
	defer conn.Close()

	selectDB, err := conn.Query("SELECT * FROM task")

	if err != nil {
		fmt.Println("Error to fetch!", err)
	}

	for selectDB.Next() {
		var task Task

		err = selectDB.Scan(&task.ID, &task.Title, &task.Description)
		tasks = append(tasks, task)

		encoder := json.NewEncoder(w)
		encoder.Encode(tasks)
	}

}

func ListTaskById(w http.ResponseWriter, r *http.Request) {
	conn := database.SetConnection()
	defer conn.Close()

	var task Task

	vars := mux.Vars(r)
	id := vars["id"]

	selectDB := conn.QueryRow("SELECT * FROM task WHERE id=" + id)
	selectDB.Scan(&task.ID, &task.Title, &task.Description)

	encoder := json.NewEncoder(w)
	encoder.Encode(task)

}

func UpdateTaskById(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusCreated)
	db := database.SetConnection()
	defer db.Close()

	var register Task

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Fprint(w, "Bad Request!")
	}

	vars := mux.Vars(r)
	id := vars["id"]

	json.Unmarshal(body, &register)

	action, err := db.Prepare("UPDATE task SET title=?, description=? where id=" + id)
	action.Exec(register.Title, register.Description)

	if err != nil {
		fmt.Fprint(w, "Error on update task!")
	}

	encoder := json.NewEncoder(w)
	encoder.Encode(register)
}

func DeleteTaskById(w http.ResponseWriter, r *http.Request) {
	db := database.SetConnection()
	defer db.Close()

	vars := mux.Vars(r)
	id := vars["id"]

	_, err := db.Exec("DELETE FROM task WHERE id=" + id)
	if err != nil {
		fmt.Fprint(w, "Error on delete task!")
	} else {

		fmt.Fprint(w, "Task deleted with successfully!")
	}
}
